import Sequelize from 'sequelize'
import dbConfig from './database.js'

const sequelize = new Sequelize(dbConfig)

const db = {}

db.sequelize = sequelize
db.Sequelize = Sequelize

export default db
