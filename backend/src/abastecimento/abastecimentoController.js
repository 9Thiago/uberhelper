import Abastecimento from './abastecimentoModel.js'

const valorKm = 1.50;

export default {
    findAll: function (req, res) {
        Abastecimento.findAll({
            include: {
                association: Abastecimento.Veiculo
            }
        }).then((dataAbastecimento) => {
            res.send(dataAbastecimento)
        })
    },

    inserirAbastecimento: function (req, res) {
        console.log(req.body)
        const abastecimento = {
            data_abastecimento: req.body.data_abastecimento,
            km_atual: req.body.km_atual,
            litros: req.body.litros,
            valor_pago: req.body.valor_pago,
            id_veiculo: req.body.id_veiculo,
            valor_km: valorKm,
        }

        Abastecimento.create(abastecimento).then((dataAbastecimento) => {
            Abastecimento.findAll({
                where: { id_veiculo: abastecimento.id_veiculo },
                limit: 1,
                offset: 1,
                subQuery: false,
                order: [
                    ["id_abastecimento", "DESC"]
                ],
            }).then((abastecimentos) => {
                if (abastecimentos != null && abastecimentos.length > 0) {
                    let abstAnterior = abastecimentos[0].dataValues
                    console.log(abastecimentos[0])
                    console.log(abstAnterior)
                    abstAnterior.km_final = abastecimento.km_atual
                    abstAnterior.km_por_litro = (abstAnterior.km_final - abstAnterior.km_atual) / abstAnterior.litros
                    Abastecimento.update(abstAnterior, { where: { id_abastecimento: abstAnterior.id_abastecimento } }).then((atualizado) => {
                        res.send(atualizado)
                    })
                }
            })
        }).catch(erro => {
            res.status(500).send(erro)
        })
    }

}
