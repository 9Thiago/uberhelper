import db from '../../configs/sequelize.js'


const sequelize = db.sequelize
const { Model, DataTypes } = db.Sequelize;

class Abastecimento extends Model { }
Abastecimento.init
    ({

        id_abastecimento: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
        },
        
        data_abastecimento: {
            type: DataTypes.DATEONLY,
            allowNull: false,
        },  
        km_atual: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        km_final: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        km_por_litro: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        litros: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        valor_km: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        valor_pago: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    }, {
        sequelize, timestamps: false, tableName: 'abastecimento'
    }
    )

export default Abastecimento