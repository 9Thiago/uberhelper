import abastecimentoController from './abastecimentoController.js'

export default (app) => {
    app.get('/abastecimento', abastecimentoController.findAll)
    app.post('/abastecimento/cadastro',abastecimentoController.inserirAbastecimento)
}
