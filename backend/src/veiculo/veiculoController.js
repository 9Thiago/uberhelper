import Veiculo from './veiculoModel.js'

export default {
    findAll : function(req,res){
        Veiculo.findAll({
            include:{
                association: Veiculo.Usuario,
                attributes: { exclude: ['password'] }
            }
        }).then((dataVeiculo)=>{
            res.send(dataVeiculo)
        })
    },

    inserirVeiculos: function(req,res){
        console.log(req.body)
        const veiculos = {
        
            modelo: req.body.modelo,
            id_usuario:req.id_usuario,
            placa: req.body.placa,
        }

        Veiculo.create(veiculos).then((dataVeiculo) => {
            res.send(dataVeiculo)
        }).catch(erro => {
            res.status(500).send(erro)
        })
    }

}




/*   where:{
                id_veiculo :1
            },
 */

/* Store.findAndCountAll({
    where: {
      name: { [Op.iLike]: nameText },
    },
    order: [["name", "ASC"]]
  }) */