import db from '../../configs/sequelize.js'
import Abastecimento from '../abastecimento/abastecimentoModel.js'

const sequelize = db.sequelize
const { Model, DataTypes } = db.Sequelize;

class Veiculo extends Model { }
Veiculo.init
    ({

        id_veiculo: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
        },
        
        modelo: {
            type: DataTypes.STRING,
            allowNull: true,
        },  
        placa: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    }, {
        sequelize, timestamps: false, tableName: 'veiculos'
    }
    )

    Abastecimento.Veiculo = Abastecimento.belongsTo(Veiculo,{
        foreignKey:{
            name:"id_veiculo"
        }
    })
    Veiculo.Abastecimento = Veiculo.hasMany(Abastecimento,{
        foreignKey:{
            name:"id_veiculo"
        }
    })


export default Veiculo