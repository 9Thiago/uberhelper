import veiculoController from './veiculoController.js'

export default (app) => {
    app.get('/veiculos', veiculoController.findAll)
    app.post('/veiculos/cadastro',veiculoController.inserirVeiculos)
}
