import usuarioController from './usuarioController.js'

export default (app) => {
    app.get('/usuarios', usuarioController.findAll)

    app.post('/usuarios', usuarioController.cadUsuario)
    app.post('/usuarios/login',usuarioController.usuarioLogin)
    app.get('/usuarios/checkLogin',usuarioController.checkLogin)
}

