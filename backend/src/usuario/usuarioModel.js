import db from '../../configs/sequelize.js'
import Veiculo from '../veiculo/veiculoModel.js'

const sequelize = db.sequelize
const { Model, DataTypes } = db.Sequelize;

class Usuario extends Model { }
Usuario.init({
    id_usuario: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
    },
    nome: {
        type: DataTypes.STRING(45),
    },
    password: {
        type: DataTypes.STRING(255),
    }
}, {
    sequelize, timestamps: false, tableName: 'usuario'
}
)

Veiculo.Usuario = Veiculo.belongsTo(Usuario,{
    foreignKey:{
        name:"id_usuario"
    }
})
Usuario.Veiculo = Usuario.hasMany(Veiculo,{
    foreignKey:{
        name:"id_usuario"
    }
})

export default Usuario