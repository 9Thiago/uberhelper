import Usuario from './usuarioModel.js'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import Veiculo from '../veiculo/veiculoModel.js';

const salt = 10;

export default {
    findAll: function (req, res) {
        Usuario.findAll().then((dataUsuario) => {
            res.send(dataUsuario)
        })
    },

    checkLogin: function (req, res) {
        let loginOk = false
        if (req.id_usuario) {
            loginOk = true
            Usuario.findOne({
                where: { id_usuario: req.id_usuario },
                attributes: { exclude: ['password'] },
                include:{
                    association:Usuario.Veiculo,
                    include:Veiculo.Abastecimento
                }
            }).then((dataUsuario) => { res.send({ loginOk: loginOk, dataUsuario: dataUsuario }) })
        } else {
            res.send({ loginOk: loginOk })
        }
    },

    usuarioLogin: function (req, res) {
        Usuario.findOne({
            where: { nome: req.body.nome }
        })
            .then((dataUsuario) => {
                if (dataUsuario) {
                    const match = bcrypt.compareSync(req.body.password, dataUsuario.password)
                    console.log(match)
                    if (match) {
                        let token = jwt.sign({ id_usuario: dataUsuario.id_usuario }, 'mysecret')
                        // res.setHeader('authToken',token)
                        res.send({ msg: 'usuario ok', authToken: token })
                    }
                    else {
                        res.status(500).send({ msg: 'Senha incorreta' })
                    }
                }
                else {
                    res.status(500).send({ msg: 'Usuário ou senha incorreta' })
                }
            }).catch((erro) => {
                console.log(erro)
                res.send(erro)
            })

    },

    cadUsuario: function (req, res) {
        const senhaCriptografada = bcrypt.hashSync(req.body.password, salt, (err, hash) => {
            if (err) {
                res.status(500).send();
                return;
            } else {
                return hash;
            }
        })
        const usuario = {
            nome: req.body.nome,
            password: senhaCriptografada,
        }

        Usuario.create(usuario).then((dataUsuario) => {
            res.send(dataUsuario)
        }).catch(erro => {
            res.status(500).send(erro)
        })

    }

}