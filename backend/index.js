import express from 'express'
import db from './configs/sequelize.js'
import abastecimentoRoutes from './src/abastecimento/abastecimentoRoutes.js'
import usuarioRoutes from './src/usuario/usuarioRoutes.js'
import veiculoRoutes from './src/veiculo/veiculoRoutes.js'
import cors from 'cors'
import bodyParser from 'body-parser'
import jwt from 'jsonwebtoken'


const app = express()
app.use(cors())
app.options('*', cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use((req, res, next) => {
    // res.append('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,authToken');
    next();
})

app.use(verificarToken)


db.sequelize.authenticate().then(() => {
    console.log('sincronizado')
}
).catch(console.log)

usuarioRoutes(app)
veiculoRoutes(app)
abastecimentoRoutes(app)

function verificarToken(req, res, next) {
    const token = req.headers.authtoken;
    if(!token || token === 'null' || token === 'undefined'){
        next();
    }
    else{
        jwt.verify(token, 'mysecret', function (err, decoded) {
            if (err) {
                return res.status(500).send('Erro ao validar o token.');
            }
    
            // se tudo estiver ok, salva no request para uso posterior
            res.setHeader('Authenticated', true);
            req.id_usuario = decoded.id_usuario; //id usr logado
            next();
        });
    }

    // if (!token || token === 'null' || token === 'undefined') return res.status(500).send('Acesso negado.');

    // jwt.verify(token, 'mysecret', function (err, decoded) {
    //     if (err) {
    //         return res.status(500).send('Erro ao validar o token.');
    //     }

    //     // se tudo estiver ok, salva no request para uso posterior
    //     res.setHeader('Authenticated', true);
    //     req.id_usuario = decoded.id_usuario;
    //     next();
    // });
}

const server = app.listen(3001, () => {
    console.log('Servidor rodando na porta ' + server.address().port + ' no host ' + server.address().address);
}
)




