import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import '../styles/global.css'
import Button from '@material-ui/core/Button';
import { useState, useEffect } from 'react'
import api from '../service/api'
import { toast } from 'react-toastify'



export default function AreaUsuario(props) {
    const [dataUsuario, setDataUsuario] = useState({})

    useEffect(() => {
        // const token=localStorage.getItem('@UberHelper/authToken')
        //{headers:{authToken:token}}
        api.get('/usuarios/checkLogin').then((resp) => {
            //console.log(resp.data)
            if (!resp.data.loginOk) {
                toast.warn('FAÇA LOGIN PARA TER ACESSO A ÁREA DE USUÁRIO', {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                props.history.push('/Login')
            } else {
                setDataUsuario(resp.data.dataUsuario)
            }

        }).catch((erro) => console.error(erro))

    }, [])

    // function calculos{

    // }

    return (
        <div className="dadosusr">
            <h1><p>Olá "{dataUsuario.nome}"</p>
                <br></br><p>  Veículos :</p>
                {dataUsuario.Veiculos != null && dataUsuario.Veiculos.length > 0 ? dataUsuario.Veiculos.map((veiculo) => {
                    return (
                        <>
                            <p key={veiculo.id_veiculo}> {veiculo.modelo} --- Placa: {veiculo.placa}</p>
                            {veiculo.Abastecimentos != null && veiculo.Abastecimentos.length > 0 ?
                                veiculo.Abastecimentos.map((abastecimento) => {
                                    return (
                                    <p key={abastecimento.id_abastecimento}> Data: {abastecimento.data_abastecimento} Consumo:{abastecimento.km_por_litro?.toFixed(2)}km/l<br></br></p>)
                                }) : <p> - - - Nenhum abastecimento cadastrado</p>
                            }</>
                    )
                })
                    : <p>Nenhum veículo cadastrado</p>
                }
            </h1>

        </div>

    )
}


