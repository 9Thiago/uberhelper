import React,{useEffect} from 'react'
import '../styles/global.css'

export default function PgLogout(props) {
    useEffect(()=>{
        localStorage.removeItem('@UberHelper/authToken')
    },[]
)
    return (
        <>
            <h1 className="h1tchau">VOCÊ FEZ LOGOUT!</h1>
            <h1 className="h1tchau"> OBRIGADO POR USAR NOSSO SERVIÇO</h1>
        </>)
}
