import Carousel from 'react-material-ui-carousel'
import Item from '../components/Item.jsx'
import './Home.css'

export default function Home() {
    
    var items = [
        {   
            name: "Cadastre se para calcular o consumo medio de combustivel e sua expectativa de lucro",
            image:"https://ubernewsroomapi.10upcdn.com/wp-content/uploads/2021/01/Uber-Driver-1-1080x540.jpg"
        },
        {
            name: "Thiago Moreira Rosa",
           image:"https://neoradar.uai.com.br/wp-content/uploads/sites/8/2021/04/uber-eats.jpg"
      
        }
    ]

   

    return (
        <div className="homediv">
            <Carousel className="carousel">
            {
                items.map( (item, i) => <Item key={i} item={item} /> )
            }
        </Carousel>


        </div>
    )
}
