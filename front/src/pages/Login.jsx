import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import './Cadastro.css'
import Button from '@material-ui/core/Button';
import {useState} from 'react'
import api from '../service/api'
import { toast } from 'react-toastify'

export default function Login(props) {
    const [nome, setNome]= useState('')
    const [password, setPassword]= useState('')

    function Login(){
        console.log(nome,password)
        const loginfo={
            nome:nome,
            password:password,
        }

        api.post('/usuarios/login', loginfo).then((resp)=>{
            localStorage.setItem('@UberHelper/authToken',resp.data.authToken)
            props.history.push('/') 
            toast.success('Login efetuado', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            })
        }
        ).catch((erro) => {
            toast.error('Usuario ou senha incorreto', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            })
        })
    }
    
    return (
        <div>
            <Grid container spacing={0}>
                <Grid item xs>
                </Grid>
                <Grid item xs={6}>
                    <Paper className="formulariocad">
                        <TextField className="InputCad" value={nome} onChange={(event)=>setNome(event.target.value)} label="Nome de usuário" variant="outlined" />
                        <TextField className="InputCad" value={password} onChange={(event)=>setPassword(event.target.value)} type="password" label="Senha" variant="outlined" />
                        <Button className="NavButtons" variant="contained" onClick={Login} >
                            Login
                        </Button>
                    </Paper>
                </Grid>
                <Grid item xs>
                </Grid>
            </Grid>

        </div>
    )
}