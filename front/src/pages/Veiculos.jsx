import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import '../styles/global.css'
import Button from '@material-ui/core/Button';
import {useState, useEffect} from 'react'
import api from '../service/api'
import {toast} from 'react-toastify'

export default function CadastroVeiculo(props) {
    const [modelo, setModelo]= useState('')
    //const [id_usuario, setIdUsuario]= useState('')
    const [placa, setPlaca]= useState('')

    useEffect(()=>{
       // const token=localStorage.getItem('@UberHelper/authToken')
        //{headers:{authToken:token}}
        api.get('/usuarios/checkLogin').then((resp)=>{
            if(!resp.data.loginOk){
                toast.warn('FAÇA LOGIN PARA TER ACESSO A ÁREA DE VEÍCULO', {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    });
                props.history.push('/Login')
            }
        }).catch((erro)=>console.error(erro))

    },[])

    function CadVeiculo(){
        console.log(modelo,placa)
        const veiculos={
            modelo:modelo,
            //id_usuario:id_usuario,
            placa:placa,
        }

        api.post('/veiculos/cadastro', veiculos).then((resp)=>{
            toast.success('VEICULO CADASTRADO', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                })
                setModelo('')
                setPlaca('')
        }).catch((erro)=>{
            toast.error('Erro ao cadastrar o veiculo', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                })
        })

    }
    return (
        <div>
            <Grid container spacing={0}>
                <Grid item xs>
                </Grid>
                <Grid item xs={6}>
                    <Paper className="formulariocad">
                        <TextField className="InputCad" value={modelo} onChange={(event)=>setModelo(event.target.value)} label="MODELO" variant="outlined" />
                        <TextField className="InputCad" value={placa} onChange={(event)=>setPlaca(event.target.value)}  label="PLACA" variant="outlined" />
                        <Button className="NavButtons" variant="contained" onClick={CadVeiculo} >
                            REGISTRAR VEICULO
                        </Button>
                    </Paper>
                </Grid>
                <Grid item xs>
                </Grid>
            </Grid>

        </div>

        
    ) 
}