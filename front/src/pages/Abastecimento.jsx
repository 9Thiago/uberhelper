import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import '../styles/global.css'
import Button from '@material-ui/core/Button';
import { useState, useEffect } from 'react'
import api from '../service/api'
import { toast } from 'react-toastify'

export default function FuncAbastecimento(props) {
    const [data_abastecimento, setData_abastecimento] = useState('')
    const [km_atual, setKm_atual] = useState('')
    const [litros, setLitros] = useState('')
    const [valor_pago, setValor_pago] = useState('')
    const [dataUsuario, setDataUsuario] = useState({})
    const [id_veiculo,setid_veiculo]=useState('')

    useEffect(() => {
        api.get('/usuarios/checkLogin').then((resp) => {
            if (!resp.data.loginOk) {
                toast.warn('FAÇA LOGIN PARA ADICIONAR UM ABASTECIMENTO', {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                props.history.push('/Login')
            }else{
                setDataUsuario(resp.data.dataUsuario)
            }
        }).catch((erro) => console.error(erro))

    }, [])

    function CadAbastecimento() {
        const abastecimento = {
            data_abastecimento: data_abastecimento,
            km_atual: km_atual,
            litros: litros,
            valor_pago: valor_pago,
            id_veiculo:id_veiculo
        }

        api.post('/abastecimento/cadastro', abastecimento).then((resp) => {
            toast.success('ABASTECIMENTO CADASTRADO', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            })
            setData_abastecimento('')
            setKm_atual('')
            setValor_pago('')
            setLitros('')
            setid_veiculo('')

        }).catch((erro) => {
            toast.error('Erro ao cadastrar o abastecimento', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            })
        })

    }
    return (
        <>
        <div>
            <Grid container spacing={0}>
                <Grid item xs>
                </Grid>
                <Grid item xs={6}>
                    <Paper className="formulariocad">
                        <TextField className="InputCad" type="date" InputLabelProps={{
                            shrink: true
                    }} value={data_abastecimento} onChange={(event) => setData_abastecimento(event.target.value)} label="Data Abastecimento" variant="outlined" />
                        <TextField className="InputCad" value={km_atual} onChange={(event) => setKm_atual(event.target.value)} label="km Atual" variant="outlined" />
                        <TextField className="InputCad" value={litros} onChange={(event) => setLitros(event.target.value)} label="Litros" variant="outlined" />
                        <TextField className="InputCad" value={valor_pago} onChange={(event) => setValor_pago(event.target.value)} label="Valor" variant="outlined" />
                        <TextField className="InputCad" value={id_veiculo} onChange={(event) => setid_veiculo(event.target.value)} label="ID Veiculo" variant="outlined" />
                        <Button className="NavButtons" variant="contained" onClick={CadAbastecimento} >
                            CADASTRAR ABASTECIMENTO
                        </Button>
                    </Paper>
                </Grid>
                <Grid item xs>
                </Grid>
            </Grid>

        </div>
                    <div className="dadosusr">
            <h1><p>  Veículos :</p>
                {dataUsuario.Veiculos != null && dataUsuario.Veiculos.length > 0 ? dataUsuario.Veiculos.map((veiculo) => {
                    return(<p key={veiculo.id_veiculo}> {veiculo.modelo} --- Placa: {veiculo.placa} --- ID Veiculo:{veiculo.id_veiculo}</p>)
                })
            :<p>Nenhum veículo cadastrado</p>}
                </h1>

                    </div>
</>
    )
}