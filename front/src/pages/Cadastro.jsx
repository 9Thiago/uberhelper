import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import '../styles/global.css'
import Button from '@material-ui/core/Button';
import {useState} from 'react'
import api from '../service/api'
import { toast } from 'react-toastify'

export default function Cadastro() {
    const [nome, setNome]= useState('')
    const [email, setEmail]= useState('')
    const [password, setPassword]= useState('')

    function CadUsuario(){
        console.log(nome,email,password)
        const usuario={
            nome:nome,
            email:email,
            password:password,
        }

        // api.post('/usuarios', usuario).then((resp)=>console.log(resp.data)).catch((erro)=>console.error(erro))
        api.post('/usuarios', usuario).then((resp) => {
            toast.success('USUARIO CADASTRADO', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            })
            setEmail('')
            setNome('')
            setPassword('')

        }).catch((erro) => {
            toast.error('Erro ao cadastrar', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            })
        })
        
    }
    return (
        <div>
            <Grid container spacing={0}>
                <Grid item xs>
                </Grid>
                <Grid item xs={6}>
                    <Paper className="formulariocad">
                        <TextField className="InputCad" value={nome} onChange={(event)=>setNome(event.target.value)} label="Nome de usuário" variant="outlined" />
                        <TextField className="InputCad" type="Email" value={email} onChange={(event)=>setEmail(event.target.value)} label="Email" variant="outlined" />
                        <TextField className="InputCad" value={password} onChange={(event)=>setPassword(event.target.value)} type="password" label="Senha" variant="outlined" />
                        <Button className="NavButtons" variant="contained" onClick={CadUsuario} >
                            Cadastrar
                        </Button>
                    </Paper>
                </Grid>
                <Grid item xs>
                </Grid>
            </Grid>

        </div>
    )
}