import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './pages/Home.jsx';
import Cadastro from './pages/Cadastro.jsx';
import Login from './pages/Login.jsx'
import CadastroVeiculo from './pages/Veiculos.jsx';
import AreaUsuario from './pages/Usuarios.jsx';
import PgLogout from './pages/Logout.jsx';
import FuncAbastecimento from './pages/Abastecimento'

function Routes() {
    return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home}></Route>
        <Route path="/Cadastro" exact component={Cadastro}></Route>
        <Route path="/Login" exact component={Login}></Route>
        <Route path="/Veiculo" exact component={CadastroVeiculo}></Route>
        <Route path="/Usuario" exact component={AreaUsuario}></Route>
        <Route path="/PgLogout" exact component={PgLogout}></Route>
        <Route path="/Abastecimento" exact component={FuncAbastecimento}></Route>

      </Switch>
    </BrowserRouter>)
  }
  
  export default Routes;