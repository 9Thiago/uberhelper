import axios from 'axios'

const api= axios.create({
    baseURL:"http://127.0.0.1:3001"
    
})

api.interceptors.request.use(function (config) {
    const authToken = window.localStorage.getItem('@UberHelper/authToken')
    if (authToken) {
      config.headers.authToken = authToken
    }
    return config
  }, function (error) {
    // Do something with request error
    return Promise.reject(error)
  })
  
export default api