import React from 'react'
import {CardMedia, Typography } from '@material-ui/core'
import './item.css'


export default function Item(props)
{
    return (
        <CardMedia
        className="Media"
        image={props.item.image}
        title={props.item.name}
      >
        <Typography className="MediaCaption">
          {props.item.name}
        </Typography>
      </CardMedia>
    )
}
