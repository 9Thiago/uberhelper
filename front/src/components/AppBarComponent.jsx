import React from 'react'
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';

export function AppBarComponent(){
return(
<div className="App">
            <AppBar className="AppBar" position="sticky">
                <Toolbar >
                    
                    <div className="HomeBtts">
                        <Button className="NavButtons" variant="contained" href="/" >
                            Home
                        </Button>
                        <Button className="NavButtons" variant="outlined" href="/Usuario" >
                            Usuário
                        </Button>
                        <Button className="NavButtons" variant="outlined" href="/Veiculo" >
                            Veículo
                        </Button>
                        <Button className="NavButtons" variant="outlined" href="/Abastecimento" >
                            Abastecimento
                        </Button>
                    </div>

                    <div className="LoginBtts">
                        <Button className="NavButtons" variant="contained" href="/Login" >
                            Logar
                        </Button>
                        <Button className="NavButtons" variant="outlined" href="/Cadastro" >
                            Cadastrar
                        </Button>
                        <Button className="NavButtons" variant="outlined" color="secondary" href="/PgLogout" >
                            Sair
                        </Button>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
)
    }