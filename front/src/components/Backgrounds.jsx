import React from 'react'
import '../styles/global.css'
import Routes from '../routes';
import { AppBarComponent } from './AppBarComponent'

function App() {
  return (
    <div className="divbg">
          <AppBarComponent style={{ flex: 0 }} />
      <div className="divroutes">
        <Routes>
        </Routes>
      </div>

    </div>);
}

export default App;
